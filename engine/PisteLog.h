// Pekka Kana 2 by Janne Kivilahti from Piste Gamez (2003-2007)
// https://pistegamez.net/game_pk2.html
//
// The public release, rewritten and continued by Carlos Donizete Froes
// is governed by a BSD-2-clause license.
//

/* DEFINITIONS --------------------------------------------------------------*/

#ifndef P_LOG
#define P_LOG

/* PROTOTYPES ---------------------------------------------------------------*/

void PisteLog_Salli_Kirjoitus();
int  PisteLog_Kirjoita(char *viesti);

#endif
